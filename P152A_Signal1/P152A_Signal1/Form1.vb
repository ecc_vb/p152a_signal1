﻿' Project:P152A_Signal1
' Auther:IE2A No.24, 村田直人
' Date: 2015年05月15日


Public Class Form1

    Dim SigImages(2) As Image                               '信号イメージを格納する配列
    Dim SigIntervals() As Integer = {2000, 1000, 3000}      '信号のインターバルを格納する配列
    Dim ButtonTexts() As String = {"スタート", "ストップ"}  'ボタンのテキスト


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        '信号のイメージを配列に格納
        SigImages(0) = My.Resources.SignalG '緑
        SigImages(1) = My.Resources.SignalY '黄色
        SigImages(2) = My.Resources.SignalR '赤

        PictureBox1.Image = SigImages(0)    '画面に青信号を表示
        Timer1.Interval = SigIntervals(0)   'タイマーのインターバル設定
        StartButton.Text = ButtonTexts(0)   'テキストを「スタート」に設定()


    End Sub

    Private Sub StartButton_Click(sender As Object, e As EventArgs) Handles StartButton.Click

        Static s As Integer '「スタート」と「ストップ」の切り替えフラグ

        'タイマーの「起動」,「停止」処理
        If s = 0 Then
            Timer1.Start()                      'タイマー起動
            s = 1                               'フラグ更新
            StartButton.Text = ButtonTexts(s)   'ボタンのテキストを設定
        ElseIf s = 1 Then
            Timer1.Stop()                       'タイマー停止
            s = 0                               'フラグ更新
            StartButton.Text = ButtonTexts(s)   'ボタンのテキストを設定
        End If

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        Static n As Integer '信号の切り替えフラグ

        '信号の画像とフラグの更新処理
        If n = 0 Then

            n = 1 'フラグを黄色に
         
        ElseIf n = 1 Then

            n = 2 'フラグを赤に

        ElseIf n = 2 Then

            n = 0 'フラグを青に

        End If

        PictureBox1.Image = SigImages(n)    'フラグのイメージを表示
        Timer1.Interval = SigIntervals(n)   'タイマーのインターバル更新

    End Sub
End Class
